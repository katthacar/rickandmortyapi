# RickAndMortyAPI

_Aplicativo que consume API de Rick y Morty:
https://rickandmortyapi.com/documentation_

## INSTRUCCIONES

_Sigue las instrucciones abajo para realizar el despliegue de forma correcta._

### Pre-requisitos

```
Tener instalado docker y docker-compose para instalar la imagen y levantar los containers
```

### Instalación y Despliegue

```
Descargar la imagen (app-rick-and-morty-api.tar) de la API del siguiente link: 
https://drive.google.com/file/d/1s9I6WYynmNKf1CcgaurBe0lhzrpfc4fe/view?usp=sharing

```
```
Descargar el archivo docker-compose.yml del siguiente link:
https://drive.google.com/file/d/1kXYZwWoaKWH-VzbDpEhNdQ8D5njorpgJ/view?usp=sharing
```

_Después de realizar la descarga de los dos archivos anteriores,
para instalar la imagen de la API, ejecutar el sig. comando desde la ubicación del archivo **app-rick-and-morty-api.tar**_
```
docker load < app-rick-and-morty-api.tar
```
_El nombre de la imagen de la API es **app-rick-and-morty-api:latest**_

_Para levantar el container del aplicativo y base de datos, desde la ubicación del 2do archivo **docker-compose.yml** 
ejecutar el sig. comando:_
```
docker-compose up
```
_El nombre de la imagen de la base de datos Postgres es **postgres:13.1-alpine**_

_Al levantar el contenedor, ya estará lista la data de prueba para hacer login y ejecutar los demás endpoints_

## Cómo usar
_Es necesario loguearse para poder acceder a los endpoints_

```
Para el Login, se debe enviar en el body el JSON con usuario y password:
Usuario con rol ROLE_SUPER_ADMIN
{
    "username": "admin",
    "password": "admin"
} 
Usuario con rol ROLE_USER
{
    "username": "basic",
    "password": "basic"
}
```
_El usuario **basic** solo tiene permitido acceder al endpoint del punto 3 (Número felíz),
mientras que el usuario **admin**, puede acceder a todos. A continuación, los Endpoints de la prueba en local:_
```
Login: POST -> http://localhost:8081/rick-morty/api/v1/auth/login
Episodes: GET -> http://localhost:8081/rick-morty/api/v1/episodes/28 (Acá se debe cambiar el id del episodio)
Número felíz: GET -> http://localhost:8081/rick-morty/api/v1/logic-tests/happy-number?numbers=99,32,78,69 (Enviar en la variable numbers otros números)
Suma de N primeros: GET -> http://localhost:8081/rick-morty/api/v1/logic-tests/plus-n-first-numbers/200 (Cambiar el último número para probar)
```

_La API también se encuentra desplegada en las siguiente URL: http://207.244.238.200:8081/rick-morty/api/v1
Abajo con los endpoints de ejemplo_
```
Endpoint del login:
POST -> http://207.244.238.200:8081/rick-morty/api/v1/auth/login
```
```
Punto 1 (APIs) Búsqueda de episodio por ID:
GET -> http://207.244.238.200:8081/rick-morty/api/v1/episodes/28
```
```
Punto 2 (Guardar en Base de datos episodios, characters y locations en cada llamada a la API y sin repetir persistencia)
Para revisar la persistencia en DB, ingresar a consola postgres:
docker exec -it app_postgres psql -U rick_morty
Desde ahí se pueden ejecutar los queries a las tablas:
episodes, characters, locations y users
```
```
Punto 3 (APIs + Lógica) Número felíz:
GET -> http://207.244.238.200:8081/rick-morty/api/v1/logic-tests/happy-number?numbers=145, 33, 331, 123, 58, 99, 32, 78, 69
```
```
Punto 4 (APIs + Test Unitario) Suma de los primeros números Naturales hasta N:
GET -> http://207.244.238.200:8081/rick-morty/api/v1/logic-tests/plus-n-first-numbers/200
```
