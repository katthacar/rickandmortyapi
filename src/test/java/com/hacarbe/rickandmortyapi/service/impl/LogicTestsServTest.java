package com.hacarbe.rickandmortyapi.service.impl;

import com.hacarbe.rickandmortyapi.dto.HappyNumberDto;
import com.hacarbe.rickandmortyapi.dto.ResponseHappyNumber;
import com.hacarbe.rickandmortyapi.dto.ResponsePlusFirstNNumbersDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JsonContentAssert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class LogicTestsServTest {

  @Autowired
  private LogicTestsServ logicTestsServ;

  @Test
  void processNumbers() {
    Integer[] happyNumbers = {331, 145, 33, 32};
    List<HappyNumberDto> happyNumbersDto = new ArrayList<>();
    happyNumbersDto.add(new HappyNumberDto(331, true));
    happyNumbersDto.add(new HappyNumberDto(145, false));
    happyNumbersDto.add(new HappyNumberDto(33, false));
    happyNumbersDto.add(new HappyNumberDto(32, true));

    ResponseHappyNumber result = logicTestsServ.processNumbers(Arrays.asList(happyNumbers));

    ResponseHappyNumber expected = new ResponseHappyNumber(happyNumbersDto);

    // Utilizo la representación en String de los objetos a comparar
    assertEquals(result.toString(), expected.toString());

  }

  @Test
  void processPlusNFirstNumbers() {
    Integer[] numbers = {5, 10, 20, 87, 96, 100, 200};

    // Cada valor del array expected, corresponde al resultado de la suma de sus primeros N números
    Integer[] expected = {15, 55, 210, 3828, 4656, 5050, 20100};

    // Se calculan los N primeros números de los valores en el array numbers
    List<Integer> result = Arrays.stream(numbers)
      .map(logicTestsServ::processPlusNFirstNumbers).collect(Collectors.toList()).stream()
      .map(ResponsePlusFirstNNumbersDto::getResult).collect(Collectors.toList());

    // Se comparan resultados (expected && result)
    assertEquals(result, Arrays.asList(expected));
  }
}