package com.hacarbe.rickandmortyapi.repository;

import com.hacarbe.rickandmortyapi.model.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRep extends JpaRepository<Character, Integer> {
}
