package com.hacarbe.rickandmortyapi.repository;

import com.hacarbe.rickandmortyapi.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRep extends JpaRepository<Location, Integer> {
}
