package com.hacarbe.rickandmortyapi.repository;

import com.hacarbe.rickandmortyapi.model.Episode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EpisodeRep extends JpaRepository<Episode, Integer> {
}
