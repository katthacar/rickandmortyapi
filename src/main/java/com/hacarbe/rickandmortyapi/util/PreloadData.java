package com.hacarbe.rickandmortyapi.util;

import com.hacarbe.rickandmortyapi.security.user.User;
import com.hacarbe.rickandmortyapi.security.user.UserRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.hacarbe.rickandmortyapi.security.constant.Authority.SUPER_ADMIN_AUTHORITIES;
import static com.hacarbe.rickandmortyapi.security.constant.Authority.USER_AUTHORITIES;
import static com.hacarbe.rickandmortyapi.security.model.Role.ROLE_SUPER_ADMIN;
import static com.hacarbe.rickandmortyapi.security.model.Role.ROLE_USER;

@Component
public class PreloadData {

  private final UserRep userRep;
  private final BCryptPasswordEncoder passwordEncoder;

  @Autowired
  public PreloadData(UserRep userRep, BCryptPasswordEncoder passwordEncoder) {
    this.userRep = userRep;
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * Se cargan dos usuarios, uno con ROLE = ROLE_SUPER_ADMIN y el otro con ROLE_USER
   */
  public void preloadUsers() {
    if (userRep.count() == 0) {
      User userAdmin = new User(
        0, "admin", passwordEncoder.encode("admin"), "Hader", "Felisan", "Carrillo",
        "Bernal", 3194614409L, "hadercarrillo@gmail.com", "Full-Stack developer",
        true, true, new Date(), null, ROLE_SUPER_ADMIN.name()//, ROLE_SUPER_ADMIN.getAuthorities()
      );
      User userBasic = new User(
        0, "basic", passwordEncoder.encode("basic"), "Hader", "Felisan", "Carrillo",
        "Bernal", 3194614409L, "hadercarrillo@outlook.com", "Full-Stack developer",
        true, true, new Date(), null, ROLE_USER.name()//, ROLE_USER.getAuthorities()
      );
      userRep.save(userAdmin);
      userRep.save(userBasic);
    }
  }

}
