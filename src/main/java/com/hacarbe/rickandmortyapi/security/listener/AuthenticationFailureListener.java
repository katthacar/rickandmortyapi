package com.hacarbe.rickandmortyapi.security.listener;

import com.hacarbe.rickandmortyapi.security.user.LoginAttemptServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFailureListener {

  private final LoginAttemptServ loginAttemptServ;

  @Autowired
  public AuthenticationFailureListener(LoginAttemptServ loginAttemptServ) {
    this.loginAttemptServ = loginAttemptServ;
  }

  @EventListener
  public void onAuthenticationFailure(AuthenticationFailureBadCredentialsEvent event) {
    Object principal = event.getAuthentication().getPrincipal();
    if (principal instanceof String) {
      String username = (String) principal;
      loginAttemptServ.addUserToLoginAttemptCache(username);
    }
  }

}