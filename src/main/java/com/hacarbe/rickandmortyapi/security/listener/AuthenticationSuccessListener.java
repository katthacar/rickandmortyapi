package com.hacarbe.rickandmortyapi.security.listener;

import com.hacarbe.rickandmortyapi.security.model.UserPrincipal;
import com.hacarbe.rickandmortyapi.security.user.LoginAttemptServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationSuccessListener {

  private final LoginAttemptServ loginAttemptServ;

  @Autowired
  public AuthenticationSuccessListener(LoginAttemptServ loginAttemptServ) {
    this.loginAttemptServ = loginAttemptServ;
  }

  @EventListener
  public void onAuthenticationSuccess(AuthenticationSuccessEvent event) {
    Object principal = event.getAuthentication().getPrincipal();
    if (principal instanceof UserPrincipal) {
      UserPrincipal user = (UserPrincipal) principal;
      loginAttemptServ.evictUserFromLoginAttemptCache(user.getUsername());
    }
  }

}