package com.hacarbe.rickandmortyapi.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.hacarbe.rickandmortyapi.security.model.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.hacarbe.rickandmortyapi.security.constant.SecurityConstant.*;
import static java.util.Arrays.stream;

@Component
public class JwtTokenProvider {

  private final Environment env;

  @Autowired
  public JwtTokenProvider(Environment env) {
    this.env = env;
  }

  /**
   * Generate JWT Token
   *
   * @param userPrincipal
   * @return token
   */
  public String generateJwtToken(UserPrincipal userPrincipal) {
    String[] claims = getClaimsFromUserPrincipal(userPrincipal);
    return JWT.create()
      .withIssuedAt(new Date())
      .withIssuer(COMPANY_NAME)
      .withAudience(COMPANY_ADMIN)
      .withSubject(userPrincipal.getUsername())
      .withArrayClaim(AUTHORITIES, claims)
      .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
      .sign(Algorithm.HMAC512(Objects.requireNonNull(env.getProperty("JWT_SECRET"))));
  }

  /**
   * Get Set of GrantedAuthority
   *
   * @param token
   * @return Set of GrantedAuthority
   */
  public List<GrantedAuthority> getAuthoritiesFromToken(String token) {
    String[] claims = getClaimsFromToken(token);
    return stream(claims).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
  }

  /**
   * Get Authentication
   *
   * @param username
   * @param authorities
   * @param request
   * @return Authentication
   */
  public Authentication getAuthentication(
    String username, List<GrantedAuthority> authorities, HttpServletRequest request
  ) {
    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
      new UsernamePasswordAuthenticationToken(username, null, authorities);
    usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
    return usernamePasswordAuthenticationToken;
  }

  /**
   * Validate if token is valid
   *
   * @param username
   * @param token
   * @return true if token is valid or false if token is not valid
   */
  public Boolean isTokenValid(String username, String token) {
    JWTVerifier jwtVerifier = getJWTVerifier();
    return StringUtils.hasText(username) && isNotTokenExpired(jwtVerifier, token);
  }

  /**
   * Get subject from token
   *
   * @param token
   * @return subject
   */
  public String getSubject(String token) {
    JWTVerifier jwtVerifier = getJWTVerifier();
    return jwtVerifier.verify(token).getSubject();
  }

  /**
   * Obtengo las authorities en formato String del UserPrincipal
   *
   * @param userPrincipal
   * @return String[] de authority
   */
  private String[] getClaimsFromUserPrincipal(UserPrincipal userPrincipal) {
    List<String> authorities = new ArrayList<>();
    for (GrantedAuthority grantedAuthority : userPrincipal.getAuthorities()) {
      authorities.add(grantedAuthority.getAuthority());
    }
    return authorities.toArray(new String[0]);
  }

  /**
   * Obtiene las authorities en formato String
   *
   * @param token
   * @return Set<String>
   */
  private String[] getClaimsFromToken(String token) {
    JWTVerifier verifier = getJWTVerifier();
    return verifier.verify(token).getClaim(AUTHORITIES).asArray(String.class);
  }

  /**
   * Verifica la firma del token
   *
   * @return JWTVerifier
   * @throws JWTVerificationException
   */
  private JWTVerifier getJWTVerifier() {
    try {
      Algorithm algorithm = Algorithm.HMAC512(Objects.requireNonNull(env.getProperty("JWT_SECRET")));
      return JWT.require(algorithm).withIssuer(COMPANY_NAME).build();
    } catch (JWTVerificationException ex) {
      throw new JWTVerificationException(TOKEN_CANNOT_BE_VERIFIED);
    }
  }

  /**
   * Valida que el token no haya expirado
   *
   * @param jwtVerifier
   * @param token
   * @return
   */
  private Boolean isNotTokenExpired(JWTVerifier jwtVerifier, String token) {
    Date expiration = jwtVerifier.verify(token).getExpiresAt();
    return expiration.after(new Date());
  }

}