package com.hacarbe.rickandmortyapi.security.config;

import com.hacarbe.rickandmortyapi.security.filter.JwtAccessDeniedHandler;
import com.hacarbe.rickandmortyapi.security.filter.JwtAuthEntryPoint;
import com.hacarbe.rickandmortyapi.security.filter.JwtAuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static com.hacarbe.rickandmortyapi.security.constant.SecurityConstant.PUBLIC_URLS;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final BCryptPasswordEncoder encoder;

  private final JwtAuthFilter jwtAuthFilter;

  private final JwtAuthEntryPoint jwtAuthEntryPoint;

  private final JwtAccessDeniedHandler jwtAccessDeniedHandler;

  private final UserDetailsService userDetailsService;

  @Autowired
  public SecurityConfig(
    BCryptPasswordEncoder encoder,
    JwtAuthFilter jwtAuthFilter,
    JwtAuthEntryPoint jwtAuthEntryPoint,
    JwtAccessDeniedHandler jwtAccessDeniedHandler,
    @Qualifier("userDetailsServ") UserDetailsService userDetailsService
  ) {
    this.encoder = encoder;
    this.jwtAuthFilter = jwtAuthFilter;
    this.jwtAuthEntryPoint = jwtAuthEntryPoint;
    this.jwtAccessDeniedHandler = jwtAccessDeniedHandler;
    this.userDetailsService = userDetailsService;
  }

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(encoder);
  }


  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().cors().and()
      .sessionManagement().sessionCreationPolicy(STATELESS)
      .and().authorizeRequests().antMatchers(PUBLIC_URLS).permitAll()
      .anyRequest().authenticated()
      .and()
      .exceptionHandling().accessDeniedHandler(jwtAccessDeniedHandler).authenticationEntryPoint(jwtAuthEntryPoint)
      .and()
      .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);
  }

  @Override
  @Bean
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

}