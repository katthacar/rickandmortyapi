package com.hacarbe.rickandmortyapi.security.user;

public interface UserServ {

  User findById(Integer id);

  User findByUsername(String username);

}