package com.hacarbe.rickandmortyapi.security.user;


import com.hacarbe.rickandmortyapi.exception.ResourceNotFoundException;
import com.hacarbe.rickandmortyapi.security.model.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

import static com.hacarbe.rickandmortyapi.security.constant.UserImplConstant.NO_USER_FOUND_BY_ID_MSG;
import static com.hacarbe.rickandmortyapi.security.constant.UserImplConstant.NO_USER_FOUND_BY_USERNAME_MSG;
import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

@Service
@Transactional
@Qualifier("userDetailsServ")
public class UserServImpl implements UserServ, UserDetailsService {

//  private final BCryptPasswordEncoder passwordEncoder;

  private final LoginAttemptServ loginAttemptServ;

  private final UserRep userRep;

  @Autowired
  public UserServImpl(
//    BCryptPasswordEncoder passwordEncoder,
    LoginAttemptServ loginAttemptServ, UserRep userRep
  ) {
//    this.passwordEncoder = passwordEncoder;
    this.loginAttemptServ = loginAttemptServ;
    this.userRep = userRep;
  }

  @Override
  public User findById(Integer id) {
    return userRep.findById(id).orElseThrow(() -> new ResourceNotFoundException(NO_USER_FOUND_BY_ID_MSG + id));
  }


  @Override
  public User findByUsername(String username) {
    return userRep.findByUsername(username).orElseThrow(
      () -> new ResourceNotFoundException(NO_USER_FOUND_BY_USERNAME_MSG + id));
  }

  @Override
  public UserDetails loadUserByUsername(String username) {
    User user = userRep.findByUsername(username)
      .orElseThrow(() -> new UsernameNotFoundException(NO_USER_FOUND_BY_USERNAME_MSG + username));
    validateLoginAttemp(user);
    user.setLastLoginDate(new Date());
    userRep.save(user);
    return new UserPrincipal(user);
  }

  private void validateLoginAttemp(User user) {
    if (user.getIsNotLocked())
      user.setIsNotLocked(!loginAttemptServ.hasExceededMaxAttempts(user.getUsername()));
    else
      loginAttemptServ.evictUserFromLoginAttemptCache(user.getUsername());
  }

}