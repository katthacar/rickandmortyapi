package com.hacarbe.rickandmortyapi.security.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
public class UserLoginDto {

  @NotBlank(message = "Debe ingresar un usuario")
  private String username;

  @NotBlank(message = "Debe ingresar un password")
  private String password;

}