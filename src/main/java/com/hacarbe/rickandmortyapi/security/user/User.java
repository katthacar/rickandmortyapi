package com.hacarbe.rickandmortyapi.security.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users", uniqueConstraints = {
  @UniqueConstraint(name = "uq_users_username", columnNames = {"username"}),
  @UniqueConstraint(name = "uq_users_email", columnNames = {"email"})
})
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false)
  private Integer id;

  @Column(name = "username", nullable = false, length = 20)
  @Basic(optional = false)
  private String username;

  @Column(name = "password", nullable = false, length = 61)
  @Basic(optional = false)
  @JsonIgnore
  private String password;

  @Column(name = "first_name", nullable = false, length = 60)
  @Basic(optional = false)
  private String firstName;

  @Column(name = "second_name", length = 60)
  private String secondName;

  @Column(name = "first_surname", nullable = false, length = 60)
  @Basic(optional = false)
  private String firstSurname;

  @Column(name = "second_surname", nullable = false, length = 60)
  @Basic(optional = false)
  private String secondSurname;

  @Column(name = "phone_number", columnDefinition = "NUMERIC(15)")
  private Long phoneNumber;

  @Column(name = "email", length = 60, nullable = false)
  @Basic(optional = false)
  private String email;

  @Column(name = "job_title", nullable = false, length = 60)
  @Basic(optional = false)
  private String jobTitle;

  @Column(name = "is_active", nullable = false, columnDefinition = "BOOLEAN DEFAULT true")
  private Boolean isActive;

  @Column(name = "is_not_locked", nullable = false, columnDefinition = "BOOLEAN DEFAULT true")
  private Boolean isNotLocked;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone = "America/Bogota")
  @Column(name = "join_date")
  private Date joinDate;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", timezone = "America/Bogota")
  @Column(name = "last_login_date")
  private Date lastLoginDate;

  private String role; // ROLE_USER{read, edit}, ROLE_ADMIN{delete}

}
