package com.hacarbe.rickandmortyapi.security.user;

import static com.hacarbe.rickandmortyapi.security.constant.SecurityConstant.JWT_TOKEN_HEADER;

import com.hacarbe.rickandmortyapi.security.jwt.JwtTokenProvider;
import com.hacarbe.rickandmortyapi.security.model.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("auth")
public class LoginCtrl {


  private final AuthenticationManager authenticationManager;
  private final JwtTokenProvider jwtTokenProvider;
  private final UserServ userServ;

  @Autowired
  public LoginCtrl(
    AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserServ userServ
  ) {
    this.authenticationManager = authenticationManager;
    this.jwtTokenProvider = jwtTokenProvider;
    this.userServ = userServ;
  }

  @PostMapping("login")
  public ResponseEntity<User> login(@RequestBody @Valid UserLoginDto userLoginDto) {
    String username = userLoginDto.getUsername();
    String password = userLoginDto.getPassword();
    authenticate(username, password);
    User loginUser = userServ.findByUsername(username);
    UserPrincipal userPrincipal = new UserPrincipal(loginUser);
    HttpHeaders jwtHeader = getJwtHeader(userPrincipal);
    return new ResponseEntity<>(loginUser, jwtHeader, HttpStatus.OK);
  }

  private void authenticate(String username, String password) {
    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
  }

  private HttpHeaders getJwtHeader(UserPrincipal userPrincipal) {
    HttpHeaders headers = new HttpHeaders();
    headers.add(JWT_TOKEN_HEADER, jwtTokenProvider.generateJwtToken(userPrincipal));
    return headers;
  }

}