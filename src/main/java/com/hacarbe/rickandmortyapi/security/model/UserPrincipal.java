package com.hacarbe.rickandmortyapi.security.model;

import com.hacarbe.rickandmortyapi.security.constant.Authority;
import com.hacarbe.rickandmortyapi.security.user.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public class UserPrincipal implements UserDetails {

  private final User user;

  public UserPrincipal(User user) {
    this.user = user;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    Role role = Role.valueOf(this.user.getRole());
    List<GrantedAuthority> authorities =
      stream(role.getAuthorities()).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    // 👇 Se agrega a la lista de GrantedAuthority el nombre del rol
    authorities.add(new SimpleGrantedAuthority(role.name()));
    return authorities;
  }

  @Override
  public String getPassword() {
    return this.user.getPassword();
  }

  @Override
  public String getUsername() {
    return this.user.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return this.user.getIsNotLocked();
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return this.user.getIsActive();
  }

}