package com.hacarbe.rickandmortyapi.security.constant;

public class SecurityConstant {

  public static final long EXPIRATION_TIME = 432_000_000; // 5 DAYS IN MILLISECONDS
  public static final String COMPANY_NAME = "hacarbe sas";
  public static final String COMPANY_ADMIN = "Management Application";
  public static final String JWT_TOKEN_HEADER = "JWT-Token";
  public static final String TOKEN_PREFIX = "Bearer ";
  public static final String OPTIONS_HTTP_METHOD = "OPTIONS";
  public static final String AUTHORITIES = "authorities";
  public static final String TOKEN_CANNOT_BE_VERIFIED = "Token no pudo ser verificado";
  public static final String FORBIDDEN_MESSAGE = "Necesita loguearse para acceder a este recurso";
  public static final String ACCESS_DENIED_MESSAGE = "No tienes permiso para acceder a este recurso";
  public static final String[] PUBLIC_URLS = {"/auth/login"};

}
