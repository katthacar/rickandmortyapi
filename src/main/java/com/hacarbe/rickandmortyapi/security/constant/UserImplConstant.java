package com.hacarbe.rickandmortyapi.security.constant;

public class UserImplConstant {

  public static String NO_USER_FOUND_BY_USERNAME_MSG = "No se encontró usuario con el usuario: ";
  public static String NO_USER_FOUND_BY_EMAIL_MSG = "No se encontró usuario con el email: ";
  public static String NO_USER_FOUND_BY_ID_MSG = "No se encontró usuario con ID: ";
  public static String EMAIL_ALREADY_EXISTS_MSG = "Correo ya se encuentra registrado en el sistema.";
  public static String USERNAME_ALREADY_EXISTS_MSG = "El usuario digitado ya se encuentra registrado en el sistema.";
  public static String USER_DELETED_SUCCESSFULLY = "Usuario ha sido borrado correctamente.";

}
