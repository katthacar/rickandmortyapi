package com.hacarbe.rickandmortyapi.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hacarbe.rickandmortyapi.exception.CustomHttpErrorResponse;;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.hacarbe.rickandmortyapi.security.constant.SecurityConstant.ACCESS_DENIED_MESSAGE;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@Component
public class JwtAccessDeniedHandler implements AccessDeniedHandler {

  @Override
  public void handle(
    HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception
  ) throws IOException {
    HttpStatus unauthorizedStatus = HttpStatus.UNAUTHORIZED;
    CustomHttpErrorResponse customHttpResponse = new CustomHttpErrorResponse(
      ACCESS_DENIED_MESSAGE, unauthorizedStatus, false
    );
    response.setContentType(APPLICATION_JSON_VALUE);
    response.setStatus(unauthorizedStatus.value());
    OutputStream outputStream = response.getOutputStream();
    val mapper = new ObjectMapper();
    mapper.writeValue(outputStream, customHttpResponse);
    outputStream.flush();
  }

}