package com.hacarbe.rickandmortyapi.security.filter;

import com.hacarbe.rickandmortyapi.security.jwt.JwtTokenProvider;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.hacarbe.rickandmortyapi.security.constant.SecurityConstant.OPTIONS_HTTP_METHOD;
import static com.hacarbe.rickandmortyapi.security.constant.SecurityConstant.TOKEN_PREFIX;

@Component
public class JwtAuthFilter extends OncePerRequestFilter {

  private final JwtTokenProvider jwtTokenProvider;

  public JwtAuthFilter(JwtTokenProvider jwtTokenProvider) {
    this.jwtTokenProvider = jwtTokenProvider;
  }

  @Override
  protected void doFilterInternal(
    HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain filterChain
  ) throws ServletException, IOException {
    if (request.getMethod().equalsIgnoreCase(OPTIONS_HTTP_METHOD))
      response.setStatus(HttpStatus.OK.value());
    else {
      String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION); // 👈 Se obtiene el header de Authorization
      // 👇 Si el header Authorization no comienza con Bearer, se deja pasar el filtro
      if (authHeader == null || !authHeader.startsWith(TOKEN_PREFIX)) {
        filterChain.doFilter(request, response);
        return;
      }
      String token = authHeader.substring(TOKEN_PREFIX.length()); // 👈 Se obtiene el token
      String username = jwtTokenProvider.getSubject(token); // 👈 Se obtiene el subject o usuario del token
      if (jwtTokenProvider.isTokenValid(username, token) && SecurityContextHolder.getContext().getAuthentication() == null) {
        List<GrantedAuthority> authorities = jwtTokenProvider.getAuthoritiesFromToken(token);
        Authentication authentication = jwtTokenProvider.getAuthentication(username, authorities, request);
        // 👇 Se agrega la autenticación al contexto de seguridad
        SecurityContextHolder.getContext().setAuthentication(authentication);
      } else {
        SecurityContextHolder.clearContext(); // 👈 Se limpia el SecurityContext
      }
    }
    filterChain.doFilter(request, response);
  }
}