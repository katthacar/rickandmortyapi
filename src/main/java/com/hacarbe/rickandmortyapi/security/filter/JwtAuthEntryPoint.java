package com.hacarbe.rickandmortyapi.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hacarbe.rickandmortyapi.exception.CustomHttpErrorResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.hacarbe.rickandmortyapi.security.constant.SecurityConstant.FORBIDDEN_MESSAGE;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@Component
public class JwtAuthEntryPoint extends Http403ForbiddenEntryPoint {

  @Override
  public void commence(
    HttpServletRequest request, HttpServletResponse response, AuthenticationException arg2
  ) throws IOException {
    CustomHttpErrorResponse httpResponse =
      new CustomHttpErrorResponse(FORBIDDEN_MESSAGE, FORBIDDEN, false);
    response.setContentType(APPLICATION_JSON_VALUE);
    response.setStatus(FORBIDDEN.value());
    OutputStream outputStream = response.getOutputStream();
    ObjectMapper mapper = new ObjectMapper();
    mapper.writeValue(outputStream, httpResponse);
    outputStream.flush();
  }
}