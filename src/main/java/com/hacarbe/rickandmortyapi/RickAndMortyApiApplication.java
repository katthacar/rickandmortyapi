package com.hacarbe.rickandmortyapi;

import com.hacarbe.rickandmortyapi.util.PreloadData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RickAndMortyApiApplication implements ApplicationRunner {

  private final PreloadData preloadData;

  @Autowired
  RickAndMortyApiApplication(PreloadData preloadData) {
    this.preloadData = preloadData;
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
    preloadData.preloadUsers();
  }

  public static void main(String[] args) {
    SpringApplication.run(RickAndMortyApiApplication.class, args);
  }

}
