package com.hacarbe.rickandmortyapi.service;

import com.hacarbe.rickandmortyapi.model.Character;

import java.util.Set;

public interface CharacterServ {

  void saveCharacter(Character character);

  void saveCharacters(Set<Character> characters);

}
