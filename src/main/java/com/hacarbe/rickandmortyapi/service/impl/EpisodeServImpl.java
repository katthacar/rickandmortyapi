package com.hacarbe.rickandmortyapi.service.impl;

import com.hacarbe.rickandmortyapi.dto.ResponseEpisodeDto;
import com.hacarbe.rickandmortyapi.exception.ResourceNotFoundException;
import com.hacarbe.rickandmortyapi.model.Character;
import com.hacarbe.rickandmortyapi.model.Episode;
import com.hacarbe.rickandmortyapi.model.Location;
import com.hacarbe.rickandmortyapi.repository.EpisodeRep;
import com.hacarbe.rickandmortyapi.service.CharacterServ;
import com.hacarbe.rickandmortyapi.service.EpisodeServ;
import com.hacarbe.rickandmortyapi.service.LocationServ;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class EpisodeServImpl implements EpisodeServ {

  private final String baseUrl = "https://rickandmortyapi.com/api/";

  private final RestTemplate restTemplate;
  private final EpisodeRep episodeRep;

  private final LocationServ locationServ;
  private final CharacterServ characterServ;

  @Autowired
  public EpisodeServImpl(
    RestTemplate restTemplate, EpisodeRep episodeRep,
    LocationServ locationServ, CharacterServ characterServ
  ) {
    this.restTemplate = restTemplate;
    this.episodeRep = episodeRep;
    this.locationServ = locationServ;
    this.characterServ = characterServ;
  }

  public ResponseEpisodeDto findById(int id) throws ResourceNotFoundException {
    try {
      Episode episode = restTemplate.getForObject(baseUrl + "/episode/" + id, Episode.class);
      assert episode != null;
      List<Character> characters = getCharacters(episode.getCharacters());
      episode.setCharacters_(characters);
      saveOnDB(episode, characters);
      return new ResponseEpisodeDto(episode.getId(), episode.getName(), characters);
    } catch (HttpClientErrorException ex) {
      throw new ResourceNotFoundException("Episode not found");
    }
  }

  private List<Character> getCharacters(@NotNull List<String> charactersURL) {
    List<String> idList =
      charactersURL.stream().map(url -> {
        int lastSlash = url.lastIndexOf('/') + 1;
        return url.substring(lastSlash);
      }).collect(Collectors.toList());
    ArrayList<Character> characters = new ArrayList<>();

    // 👇 Por cada id de los personajes, se busca su información completa
    idList.forEach(id -> {
      Character character = restTemplate.getForObject(baseUrl + "/character/" + id, Character.class);
      assert character != null;
      // 👇 Si el personaje no tiene Location, no se hace la búsqueda de esta
      if (!character.getLocation().getUrl().isBlank()) {
        Location location = getLocationByUrl(character.getLocation().getUrl());
        character.setLocation(location);
      }
      // 👇 Si el personaje no tiene Origen (Location), no se hace la búsqueda de este
      if (!character.getOrigin().getUrl().isBlank()) {
        Location origin = getLocationByUrl(character.getOrigin().getUrl());
        character.setOrigin(origin);
      }
      characters.add(character);
    });
    return characters;
  }

  private Location getLocationByUrl(String url) {
    return restTemplate.getForObject(url, Location.class);
  }

  /**
   * Se almacenan en DB el episodio, personaje y ubicación, la ubicación se obtiene del personaje
   *
   * @param episode
   * @param characters
   */
  private void saveOnDB(Episode episode, List<Character> characters) {
    Set<Location> origins = characters.stream().map(Character::getOrigin).collect(Collectors.toSet());
    Set<Location> locations = characters.stream().map(Character::getLocation).collect(Collectors.toSet());
    locations.addAll(origins);
    locationServ.saveLocations(locations);
    characterServ.saveCharacters(new HashSet<>(characters));
    saveEpisode(episode);
  }

  @Override
  public void saveEpisode(Episode episode) {
    // 👇 Solo se guarda en DB si el episodio no ha sido creado
    if (!episodeRep.existsById(episode.getId())) episodeRep.save(episode);
  }

}
