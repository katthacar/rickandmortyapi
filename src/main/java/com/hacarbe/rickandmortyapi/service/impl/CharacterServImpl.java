package com.hacarbe.rickandmortyapi.service.impl;

import com.hacarbe.rickandmortyapi.model.Character;
import com.hacarbe.rickandmortyapi.repository.CharacterRep;
import com.hacarbe.rickandmortyapi.service.CharacterServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CharacterServImpl implements CharacterServ {

  private final CharacterRep characterRep;

  @Autowired
  public CharacterServImpl(CharacterRep characterRep) {
    this.characterRep = characterRep;
  }

  @Override
  public void saveCharacter(Character character) {
    // 👇 Solo se guarda en DB si el personaje no ha sido creado
    if (!characterRep.existsById(character.getId())) characterRep.save(character);
  }

  @Override
  public void saveCharacters(Set<Character> characters) {
    characters.forEach(this::saveCharacter);
  }

}
