package com.hacarbe.rickandmortyapi.service.impl;

import com.hacarbe.rickandmortyapi.dto.HappyNumberDto;
import com.hacarbe.rickandmortyapi.dto.ResponseHappyNumber;
import com.hacarbe.rickandmortyapi.dto.ResponsePlusFirstNNumbersDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class LogicTestsServ {

  /**
   * Verifica que el número sea o no un número felíz
   *
   * @param number número a validar si es felíz o no
   * @return falso o verdadero
   */
  private Boolean isHappyNumber(int number) {
    Set<Integer> digits = new HashSet<>();
    while (digits.add(number)) {
      int result = 0;
      while (number > 0) {
        int mod = number % 10; // Obtenemos el módulo del número
        result += Math.pow(mod, 2); // Se suma el cuadrado el módulo anterior a un resultado parcial
        number = number / 10; // En cada iteración toma los primeros dígitos hasta que el dígito obtenido sea <= 0
      }
      number = result;
    }
    return number == 1;
  }

  /**
   * Calcula la suma de los primeros N números
   *
   * @param number número a calcular
   * @return suma total
   */
  private int plusNFirstNumbers(int number) {
    return (number + 1) * number / 2; // Fórmula de Gauss (x + 1) * x / 2
  }

  public ResponseHappyNumber processNumbers(List<Integer> numbers) {
    List<HappyNumberDto> happyNumbers = new ArrayList<>();
    numbers.forEach(number -> {
      happyNumbers.add(new HappyNumberDto(number, isHappyNumber(number)));
    });
    return new ResponseHappyNumber(happyNumbers);
  }

  public ResponsePlusFirstNNumbersDto processPlusNFirstNumbers(int number) {
    return new ResponsePlusFirstNNumbersDto(plusNFirstNumbers(number));
  }

//  public static void main(String[] args) {
//    int[] happyNumbers = {145, 33, 331, 123, 58, 99, 32, 78, 69};
//    for (int n : happyNumbers) {
//      if (isHappyNumber(n)) System.out.println(n + " Happy number");
//      else System.out.println(n + " Not Happy number");
//    }
//  }

}
