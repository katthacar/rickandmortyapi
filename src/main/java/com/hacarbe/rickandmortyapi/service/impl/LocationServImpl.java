package com.hacarbe.rickandmortyapi.service.impl;

import com.hacarbe.rickandmortyapi.model.Location;
import com.hacarbe.rickandmortyapi.repository.LocationRep;
import com.hacarbe.rickandmortyapi.service.LocationServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class LocationServImpl implements LocationServ {

  private final LocationRep locationRep;

  @Autowired
  public LocationServImpl(LocationRep locationRep) {
    this.locationRep = locationRep;
  }

  @Override
  public void saveLocation(Location location) {
    // 👇 Solo se guarda en DB si la ubicación no ha sido creada
    if (!locationRep.existsById(location.getId())) locationRep.save(location);
  }

  @Override
  public void saveLocations(Set<Location> locations) {
    locations.forEach(this::saveLocation);
  }
}
