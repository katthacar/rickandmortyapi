package com.hacarbe.rickandmortyapi.service;

import com.hacarbe.rickandmortyapi.model.Location;

import java.util.Set;

public interface LocationServ {

  void saveLocation(Location location);

  void saveLocations(Set<Location> locations);

}
