package com.hacarbe.rickandmortyapi.service;

import com.hacarbe.rickandmortyapi.dto.ResponseEpisodeDto;
import com.hacarbe.rickandmortyapi.model.Episode;

public interface EpisodeServ {

  ResponseEpisodeDto findById(int id);

  void saveEpisode(Episode episode);

}
