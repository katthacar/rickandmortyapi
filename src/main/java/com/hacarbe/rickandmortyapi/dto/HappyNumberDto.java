package com.hacarbe.rickandmortyapi.dto;

import lombok.*;

/**
 * DTO correspondiente a la respuesta de tipo happyNumber, Ej:
 * {
 * "number": 145,
 * "happy": false
 * }
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class HappyNumberDto {

  @Getter
  @Setter
  int number;

  @Getter
  @Setter
  boolean isHappy;

}
