package com.hacarbe.rickandmortyapi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class ResponsePlusFirstNNumbersDto {

  @Getter
  @Setter
  int result;

}
