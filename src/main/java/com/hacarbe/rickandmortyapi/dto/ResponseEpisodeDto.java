package com.hacarbe.rickandmortyapi.dto;

import com.hacarbe.rickandmortyapi.model.Character;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class ResponseEpisodeDto {

  @Getter
  @Setter
  private int episode;

  @Getter
  @Setter
  private String episodeName;

  @Getter
  @Setter
  private List<Character> characters;

}
