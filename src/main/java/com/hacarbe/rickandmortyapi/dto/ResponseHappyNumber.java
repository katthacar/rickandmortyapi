package com.hacarbe.rickandmortyapi.dto;

import lombok.*;

import java.util.List;

/**
 * DTO para la respuesta de la petición al endpoint rick-morty/api/v1/logic-tests/happy-number?numbers=1,2,3...
 * Ej:
 * {
 * "numbers": [
 * {
 * "number": 145,
 * "happy": false
 * },
 * {
 * "number": 331,
 * "happy": true
 * }...
 * ]
 * }
 */
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResponseHappyNumber {

  @Getter
  @Setter
  List<HappyNumberDto> numbers;

}
