package com.hacarbe.rickandmortyapi.exception;

import com.auth0.jwt.exceptions.TokenExpiredException;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private static final String ACCOUNT_LOCKED = "Tu cuenta ha sido bloqueada. Por favor, contacta con el admin del sistema.";
  private static final String METHOD_IS_NOT_ALLOWED = "Esta petición no está permitida. Por favor envía una petición '%s'.";
  private static final String INTERNAL_SERVER_ERROR_MSG = "Un error ocurrió mientras se procesaba la petición.";
  private static final String ERROR_PROCESSING_FILE = "Ocurrió un error mientras se procesaba el archivo.";
  private static final String NOT_ENOUGH_PERMISSION = "No tienes suficientes permisos.";
  private static final String ERROR_PATH = "/error";
  private static final String ACCOUNT_DISABLED =
    "Tu cuenta ha sido deshabilitata. Si crees que esto es un error, contacta on el admin del sistema.";
  private static final String INCORRECT_CREDENTIALS =
    "Usuario y/o Password incorrectos. Verifique sus credenciales e intente de nuevo.";

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<Object> resourceNotFoundException(ResourceNotFoundException ex) {
    return createHttpErrorResponse(NOT_FOUND, ex.getMessage());
  }

  @ExceptionHandler(NoResultException.class)
  public ResponseEntity<Object> notResultException(NoResultException ex) {
    return createHttpErrorResponse(NOT_FOUND, ex.getMessage());
  }

  @ExceptionHandler(DisabledException.class)
  public ResponseEntity<Object> accountDisableException() {
    return createHttpErrorResponse(BAD_REQUEST, ACCOUNT_DISABLED);
  }

  @ExceptionHandler(BadCredentialsException.class)
  public ResponseEntity<Object> badCredentialsException() {
    return createHttpErrorResponse(BAD_REQUEST, INCORRECT_CREDENTIALS);
  }

  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<Object> accessDeniedException() {
    return createHttpErrorResponse(FORBIDDEN, NOT_ENOUGH_PERMISSION);
  }

  @ExceptionHandler(LockedException.class)
  public ResponseEntity<Object> lockedException() {
    return createHttpErrorResponse(UNAUTHORIZED, ACCOUNT_LOCKED);
  }

  @ExceptionHandler(TokenExpiredException.class)
  public ResponseEntity<Object> tokenExpiredException(TokenExpiredException ex) {
    return createHttpErrorResponse(UNAUTHORIZED, ex.getMessage());
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<Object> illegalArgumentException(IllegalArgumentException ex) {
    return createHttpErrorResponse(BAD_REQUEST, ex.getMessage());
  }

  @ExceptionHandler(IOException.class)
  public ResponseEntity<Object> ioException(IOException ex) {
    logger.error(ex.getMessage());
    return createHttpErrorResponse(INTERNAL_SERVER_ERROR, ex.getMessage());
  }

  @Override
  public ResponseEntity<Object> handleHttpRequestMethodNotSupported(
    HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request
  ) {
    HttpMethod supportedMethod = Objects.requireNonNull(ex.getSupportedHttpMethods().iterator().next());
    return createHttpErrorResponse(METHOD_NOT_ALLOWED, String.format(METHOD_IS_NOT_ALLOWED, supportedMethod));
  }

  @Override
  public ResponseEntity<Object> handleMethodArgumentNotValid(
    MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request
  ) {
    List<String> msgErrors =
      ex.getBindingResult().getAllErrors().stream().map(it -> it.getDefaultMessage()).collect(Collectors.toList());
    return createHttpErrorResponse(BAD_REQUEST, msgErrors.toString());
  }

  @Override
  public ResponseEntity<Object> handleHttpMessageNotReadable(
    HttpMessageNotReadableException ex, HttpHeaders headers,
    HttpStatus status, WebRequest request
  ) {
    return createHttpErrorResponse(BAD_REQUEST, ex.getMessage());
  }

  private ResponseEntity<Object> createHttpErrorResponse(HttpStatus httpStatus, String message) {
    val customHttpResponseApp = new CustomHttpErrorResponse(message, httpStatus, false);
    return new ResponseEntity<Object>(customHttpResponseApp, httpStatus);
  }

}
