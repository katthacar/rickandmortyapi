package com.hacarbe.rickandmortyapi.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@NoArgsConstructor
@AllArgsConstructor
public class CustomHttpErrorResponse {

  @Getter
  private String reason;

  @Getter
  private HttpStatus httpStatus;

  @Getter
  private Boolean ok;

}
