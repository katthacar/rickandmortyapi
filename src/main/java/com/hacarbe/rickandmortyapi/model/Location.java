package com.hacarbe.rickandmortyapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "locations")
public class Location extends ParentEntity {

  @Getter
  @Setter
  @Column
  private String type;

  @Getter
  @Setter
  @Column
  private String dimension;

  @Getter
  @Setter
  @ElementCollection
  private List<String> residents;

}
