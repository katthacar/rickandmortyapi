package com.hacarbe.rickandmortyapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "episodes")
public class Episode extends ParentEntity {

  @Getter
  @Setter
  @JsonProperty(value = "air_date")
  @Column(name = "air_date")
  private String airDate;

  @Getter
  @Setter
  @Column
  private String episode;

  @Getter
  @Setter
  @Transient
  private List<String> characters;

  @Getter
  @Setter
  @ManyToMany
  @JoinTable(
    name = "episodes_characters",
    joinColumns = {@JoinColumn(name = "episode_id")}, inverseJoinColumns = {@JoinColumn(name = "character_id")},
    foreignKey = @ForeignKey(name = "fk_episodes_characters"), inverseForeignKey = @ForeignKey(name = "fk_characters_episodes"),
    uniqueConstraints = {@UniqueConstraint(name = "uq_episodes_characters", columnNames = {"episode_id", "character_id"})}
  )
  private List<Character> characters_;

}
