package com.hacarbe.rickandmortyapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@EqualsAndHashCode
@MappedSuperclass
public abstract class ParentEntity {

  @Getter
  @Setter
  @Id
  @Column(name = "id", updatable = false)
  private int id;

  @Getter
  @Setter
  @Column
  private String name;

  @Getter
  @Setter
  @Column
  private String url;

  @Getter
  @Setter
  @Column
  private String created;

}
