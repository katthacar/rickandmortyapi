package com.hacarbe.rickandmortyapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "characters")
public class Character extends ParentEntity{

  @Getter
  @Setter
  @Column
  private String status;

  @Getter
  @Setter
  @Column
  private String species;

  @Getter
  @Setter
  @Column
  private String type;

  @Getter
  @Setter
  @Column
  private String gender;

  @Getter
  @Setter
  @Column
  private String image;

  @Getter
  @Setter
  @ManyToOne
  @JoinColumn(
    name = "origin", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_character_origin")
  )
  private Location origin;

  @Getter
  @Setter
  @ManyToOne
  @JoinColumn(
    name = "location", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_character_location")
  )
  private Location location;

  @Getter
  @Setter
  @Transient
  private List<String> episode;

}
