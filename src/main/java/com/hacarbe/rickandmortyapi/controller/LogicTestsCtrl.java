package com.hacarbe.rickandmortyapi.controller;

import com.hacarbe.rickandmortyapi.dto.ResponseHappyNumber;
import com.hacarbe.rickandmortyapi.dto.ResponsePlusFirstNNumbersDto;
import com.hacarbe.rickandmortyapi.service.impl.LogicTestsServ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("logic-tests")
public class LogicTestsCtrl {

  private final LogicTestsServ logicTestServ;

  @Autowired
  public LogicTestsCtrl(LogicTestsServ logicTestServ) {
    this.logicTestServ = logicTestServ;
  }

  @PreAuthorize("hasAnyAuthority('user:read', 'user:create', 'user:update', 'user:delete')")
  @GetMapping("happy-number")
  public ResponseEntity<ResponseHappyNumber> happyNumber(@RequestParam(value = "numbers") List<Integer> numbers) {
    return new ResponseEntity<>(logicTestServ.processNumbers(numbers), HttpStatus.OK);
  }

  @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
  @GetMapping("plus-n-first-numbers/{number}")
  public ResponseEntity<ResponsePlusFirstNNumbersDto> plusNfirstNumbers(@PathVariable("number") int number) {
    return new ResponseEntity<>(logicTestServ.processPlusNFirstNumbers(number), HttpStatus.OK);
  }

}
