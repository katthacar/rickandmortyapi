package com.hacarbe.rickandmortyapi.controller;

import com.hacarbe.rickandmortyapi.dto.ResponseEpisodeDto;
import com.hacarbe.rickandmortyapi.exception.ResourceNotFoundException;
import com.hacarbe.rickandmortyapi.service.impl.EpisodeServImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("episodes")
public class EpisodeCtrl {

  private final EpisodeServImpl episodeServImpl;

  @Autowired
  public EpisodeCtrl(EpisodeServImpl episodeServImpl) {
    this.episodeServImpl = episodeServImpl;
  }


  @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
  @GetMapping("{id}")
  public ResponseEntity<ResponseEpisodeDto> findById(@PathVariable("id") int id) throws ResourceNotFoundException {
    return new ResponseEntity<>(episodeServImpl.findById(id), HttpStatus.OK);
  }

}
