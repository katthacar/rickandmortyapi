FROM openjdk:17-jdk-alpine
MAINTAINER hacarbe.com
ARG JAR_FILE=target/rickAndMortyApi-1.0.0.jar
COPY ${JAR_FILE} rickAndMortyApi-1.0.0.jar
ENTRYPOINT ["java", "-jar", "-DJWT_SECRET=SoM3_alEatory_W0rdsFor_JWT_S3CreTS", "rickAndMortyApi-1.0.0.jar"]
#RUN addgroup -S spring && adduser -S spring -G spring
#USER spring:spring

#FROM library/postgres
#ENV POSTGRES_USER=rick_morty
#ENV POSTGRES_PASSWORD=rick_morty
#ENV POSTGRES_DB=rick_morty
#COPY init.sql /docker-entrypoint-initdb.d/
